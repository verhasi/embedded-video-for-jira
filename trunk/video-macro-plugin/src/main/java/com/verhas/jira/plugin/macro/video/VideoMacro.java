package com.verhas.jira.plugin.macro.video;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

abstract class VideoMacro extends BaseMacro
{
    abstract String getTemplateName();
    
    private final TemplateRenderer renderer;

    public VideoMacro(TemplateRenderer templateRenderer) {
        this.renderer = templateRenderer;
    }

    @Override
    public boolean isInline() {
        return true;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.INLINE;
    }

    @Override
    public String execute(Map map, String string, RenderContext rc) throws MacroException {
        try {
            map.put("videoid",map.get("0"));
            StringWriter stringWriter = new StringWriter();
            renderer.render(getTemplateName(), map, stringWriter);
            return stringWriter.toString();
        } catch (RenderingException ex) {
            throw new MacroException(ex);
        } catch (IOException ex) {
            throw new MacroException(ex);
        }
    }
}
