/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.verhas.jira.plugin.macro.video;

import com.atlassian.templaterenderer.TemplateRenderer;

/**
 *
 * @author verhasi
 */
public class VimeoVideoMacro extends VideoMacro {

    public VimeoVideoMacro(TemplateRenderer templateRenderer) {
        super(templateRenderer);
    }

    @Override
    String getTemplateName() {
        return "templates/vimeo.vm";
    }
}
