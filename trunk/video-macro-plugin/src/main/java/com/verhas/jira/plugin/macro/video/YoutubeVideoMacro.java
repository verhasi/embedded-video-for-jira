/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.verhas.jira.plugin.macro.video;

import com.atlassian.templaterenderer.TemplateRenderer;

/**
 *
 * @author verhasi
 */
public class YoutubeVideoMacro extends VideoMacro {
    
    public YoutubeVideoMacro(TemplateRenderer templateRenderer) {
        super(templateRenderer);
    }
    
    @Override
    String getTemplateName() {
        return "templates/youtube.vm";
    }
}
